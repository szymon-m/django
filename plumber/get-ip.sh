#!/bin/bash

#ifconfig eth0 | grep "inet " | awk -F'[: ]+' '{print $4}' > django_ip

#ip addr show eth0 | grep -Po 'inet \K[\d.]+' > django/mysite/plumber_ip

#echo '##>>> 2 Plumber ip:'
#cat django/mysite/plumber_ip

ip addr show eth0 | grep -Po 'inet \K[\d.]+' | sed -e "s/\(.*\)/'\1'/" > ip
echo PLUMBER_IP = > head
paste head ip >> django/mysite/mysite/settings.py
rm ip
rm head
