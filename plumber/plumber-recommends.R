library("recommenderlab")

#' @filter cors
cors <- function(res) {
    res$setHeader("Access-Control-Allow-Origin", "*")
    plumber::forward()
}

#' Return predictions for specified user id
#' @get /recommends/<id:int>

function(id) {

   data("MovieLense")

   ### use only users with more than 100 ratings
   MovieLense100 <- MovieLense[rowCounts(MovieLense)>100,]

   train <- MovieLense[1:50]
   rec <- Recommender(train, method = "UBCF")

   pre <- predict(rec, MovieLense[id])
   ready <- as(pre, "list")

}
