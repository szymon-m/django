R recommendation service added:

using 
- recommenderlab  (R library - https://github.com/cran/recommenderlab)
   for recommendations computed from example dataset MovieLense
   
- plumber (R library - https://www.rplumber.io/)
   for exposing web API for computed recommendations 
   (as a wrapper function for recommendation script)
   
   