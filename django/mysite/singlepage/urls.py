from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [

    path('', views.index, name='spa_index'),
    path('/json', views.json, name='spa_json'),
    path('/reload', views.reload, name='spa_reload')
]
