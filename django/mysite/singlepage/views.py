from django.shortcuts import render


def index(request):
    context = {'helloworld': 'Wybierz ID użytkownika do rekomendacji: '}
    return render(request, 'singlepage/index.html', context)


def json(request):
    # json_response = JsonResponse({'first_branch':
    #                 {
    #                     'element_one': 'first',
    #                     'element_two': 'two'
    #                 }
    #           })

    json_response = {'first_branch':
        {
            'element_one': 'first',
            'element_two': 'two'
        }
    }

    # context = {'json_response': json_response}
    context = {'json_response': json_response}
    return render(request, 'singlepage/json.html', context)


def reload(request):

    selected_id = request.POST['user_selector']
    context = {'selected_item': selected_id}

    return render(request, 'singlepage/reload.html', context)
