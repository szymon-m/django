function dummy() {

        let gitHubRepo = "https://api.github.com/users/szymon-m/repos";

        let selected = document.getElementById("users");
        let selected_id = selected.options[selected.selectedIndex].value;

        // answer found on : https://stackoverflow.com/questions/49213876/send-ajax-request-in-front-end-js-from-one-container-to-another

	// url findings :
        // working for Docker on Windows
	// working for Docker on Linux

        let other_url = document.location.protocol + "//" + document.location.hostname + ":8001/recommends/" + selected_id;

        //let linuxPlumber = "http://localhost:8001/recommends/"+selected_id;
        let windowsPlumber = other_url;
        let linuxPlumber = other_url;

        console.log(other_url);
        // 0. WORKING EXAMPLE OF JSON WITH PLUMBER

        $.ajax({
            url: linuxPlumber,
            crossOrigin: true,
            type: 'get',
            dataType: 'json',
            success: function (data) {

                var id,title, film, user;
                var recomendation = [];
                recomendation.push("<p>Films recommended for user: "+ selected_id + "<br><br>");

                 for(films in data) {

                    for(title in data[films]) {

                        recomendation.push("- " + data[films][title] + "<br>");
                    }
                 }

                recomendation.push("</p>");
                document.getElementById("content").innerHTML = recomendation.join("");
                console.log(recomendation);
            }
        });

        $.ajax({
            url: windowsPlumber,
            crossOrigin: true,
            type: 'get',
            dataType: 'json',
            success: function (data) {

                var id,title, film, user;
                var recomendation = [];
                recomendation.push("<p>Films recommended for user: "+ selected_id + "<br><br>");

                 for(films in data) {

                    for(title in data[films]) {

                        recomendation.push("- " + data[films][title] + "<br>");
                    }
                 }

                recomendation.push("</p>");
                document.getElementById("content").innerHTML = recomendation.join("");
                console.log(recomendation);
            }
        });
        // 1. WORKING EXAMPLE OF JSON

        // $.ajax({
        //     url: gitHubRepo,
        //     type: 'get',
        //     dataType: 'json',
        //     success: function (data) {
        //
        //         var x;
        //         var repos = [];
        //         repos.push("<p>Repositories: ");
        //
        //         // to access actual selected value of dropdown
        //         var selected = document.getElementById("users");
        //         var selected_id = selected.options[selected.selectedIndex].value;
        //         repos.push("<br>(Option selected : " + selected_id + ")<br>");
        //
        //         for(x in data) {
        //
        //             repos.push("- " + data[x].name + "<br>");
        //
        //         }
        //         repos.push("</p>");
        //         document.getElementById("content").innerHTML = repos.join("");
        //
        //     }
        // });



        // 2nd WORKING EXAMPLE

        // $.getJSON(gitHubRepo, function (data) {
        //
        //
        //     var items = [];
        //     $.each(data, function (key, val) {
        //         items.push("<li id='" + key + "'>" + val + "</li>");
        //
        //     });
        //
        //     $("<ul/>" , { "class": "my-new-list" ,
        //         html: items.join("")
        //     }).appendTo(".content")
        // });



}



