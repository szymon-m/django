#!/bin/bash

#ifconfig eth0 | grep "inet " | awk -F'[: ]+' '{print $4}' > django_ip

#ip addr show eth0 | grep -Po 'inet \K[\d.]+' > django/mysite/django_ip

#echo '##>>> 1 Django ip:'
#cat django/mysite/django_ip

ip addr show eth0 | grep -Po 'inet \K[\d.]+' | sed -e "s/\(.*\)/'\1'/" > ip
echo DJANGO_IP = > head
paste head ip >> django/mysite/mysite/settings.py
rm ip
rm head

