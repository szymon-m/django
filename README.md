**Django - Recommenderlab - Plumber.io project**
=


#### *Aplikacja przedstawia rekomendacje kilku filmów dla użytkownika o danym identyfikatorze (id).*
 

 `[     R     ]` Biblioteka [plumber.io](https://www.rplumber.io/) tworzy API dla rekomendacji z użyciem biblioteki [recommenderlab](https://github.com/mhahsler/recommenderlab) na przykładowej bazie MovieLense.
 
 `[Python/Javascript]` Sama strona zbudowana jest na podstawie frameworku [Django](https://www.djangoproject.com/) oraz Javascript/JQuery


#### Uruchomienie

 * [ ] instalujemy Docker'a (do zainstalowania i uruchomienia Docker'a może być potrzebne uruchomienie wirtualizacji - szczegóły tutaj ->> https://docs.docker.com/docker-for-windows/install/#what-to-know-before-you-install)
 * [ ] klonujemy repozytorium do wybranego katalogu `git clone https://gitlab.com/szymon-m/django.git`
 * [ ] w systemie Windows uruchamiamy zainstalowanego Docker'a tj. Docker Quickstart (otworzy się okno terminala) 
 * [ ] przechodzimy do katalogu, gdzie sklonowaliśmy repozytorium (jeżeli to katalog domowy / Dokumenty - to przechodzimy do katalogu poprzez `cd ~/Documents/django` gdzie `~` to katalog domowy)
 * [ ] uruchamiamy poleceniem `docker-compose up`
 * [ ] **`DOSTĘP DO APLIKACJI:`** *przy uruchomieniu Docker Quickstart w systemie windows podany jest domyślny adres IP 
 dla Dockera np. http://192.168.99.100 . Aplikację uruchamiamy właśnie z tego adresu + nr portu (dla Django to port 8000) + /spa czyli np. http://192.168.99.100:8000/spa
 bądź też http://127.0.0.1:8000/spa lub http://localhost:8000/spa*
 
 * [ ] *Dostęp do samego API rekomendacji (plumber+recommenderlab) to lokalny host jw. + port 8001 + /recommends/[ID użytkownika] np. http://192.168.99.100:8001/recommends/100*